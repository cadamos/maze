package edu.hawaii.ics211.maze;

/**
 * BrandenOgata6.java
 * Copyright (c) Branden Ogata 2016
 */

import java.util.Scanner;

/**
*	@author 	Branden Ogata
*
*	Date:		20 February 2008 - 3 March 2008
*
*	File Name:	BrandenOgata6.java
*
*	Description: 	Combine everything you've learned so far to write a very simple maze game.
*			(� Whoever writes these things on the ICS 111 website)
*
*	Features:	Prints border around maze
*			Places exit on a random wall, not always east
*			Calculates the "best" move if the user requests
*			Three difficulty levels
*				Easy: No additional features
*				Medium: Traps, limited vision
*				Hard: Traps, limited vision, moving monsters
*				Extra Hard: Traps, limited vision, moving monsters, moving exit, moving rival
*					(Yes, this is more than three difficulty levels.  I cannot count.)
*			
*/

public class BrandenOgata6
{
	public static final int MAZE_HEIGHT = 10;
	public static final int MAZE_WIDTH = 10;
	
	// playerCoordinate and exitCoordinate are composites of the x and y coordinates for each:
	// (100 * X) + (Y)
	// This eliminates any potential confusion between x and y coordinates,
	// as well as with && or || logic.
	// The trap, enemy, and rival variables are based on the same principle.
	// However, these other variables must be initialized to some negative value,
	// else they might accidentally appear on the map when they are not supposed to.
	public static int playerCoordinate = 0;
	public static int exitCoordinate = 0;
	public static int trap0 = -1;
	public static int trap1 = -1;
	public static int trapCount = 0;
	public static int radarUses = -1;
	public static int enemy0 = -1;
	public static int enemy1 = -1;
	public static int rival = -2;
	public static int rivalTrapCount = 0;
	public static int bestMoves = 0;

	// For the cheats... er, "debugging codes"
	public static boolean e = false;
	public static boolean d = false;
	
	public static void main(String[] args)
	{
		Scanner input = new Scanner(System.in);	
		int playerX = 0;
		int playerY = 0;
		int exitSide = 0;
		int exitX = 0;
		int exitY = 0;
		int counter = 0;
		int mode = -1;
		String modeInput = "";
		String userInput = "";
		boolean playDead = false;
		int deathType = -1;

		// Assigns random coordinates to player and exit
		do
		{
			playerX = getRandom('x');
			playerY = getRandom('y');
			
			// Determines which wall the exit will be on
			exitSide = (playerX + playerY) % 4;
			if (exitSide == 0)
			{
				exitX = 0;
				exitY = getRandom('y');
			}
			else if (exitSide == 1)
			{
				exitX = MAZE_WIDTH - 1;
				exitY = getRandom('y');
			}
			else if (exitSide == 2)
			{
				exitX = getRandom('x');
				exitY = 0;
			}
			else if (exitSide == 3)
			{
				exitX = getRandom('x');
				exitY = MAZE_HEIGHT - 1;
			}			
		} while ((playerX == exitX) && (playerY == exitY));
		
		playerCoordinate = (100 * playerX) + playerY;
		exitCoordinate = (100 * exitX) + exitY;
		bestMoves = Math.abs(playerX - exitX) + Math.abs(playerY - exitY);
		
		// If they do not maximize the screen, then they will miss a fair deal of instructions.
		// Also, if MAZE_HEIGHT especially is changed, the maze might become too large to fit on a smaller window.
		System.out.println("");
		System.out.println("For optimal gameplay, we recommend that you maximize your command prompt now.");
		System.out.println("");

		// Allows user to select from the three difficulty levels
		do
		{
			System.out.println("Select a difficulty level from the list below (1/2/3), then press Enter:");
			System.out.println("1: Easy: A simple " + MAZE_HEIGHT + " * " + MAZE_WIDTH + " maze.");
			System.out.println("2: Medium: The aforementioned " + MAZE_HEIGHT + " * " + MAZE_WIDTH + 
						" maze, which is no longer simple.");
			System.out.println("3: Hard: An extremely difficult " + 
						MAZE_HEIGHT + " * " + MAZE_WIDTH + " maze.");
			modeInput = input.nextLine();

			if ((modeInput.equals("1")) || (modeInput.equalsIgnoreCase("easy")))
			{
				mode = 1;
			}
			else if ((modeInput.equals("2")) || (modeInput.equalsIgnoreCase("medium")))
			{
				mode = 2;
			}
			else if ((modeInput.equals("3")) || (modeInput.equalsIgnoreCase("hard")))
			{
				mode = 3;
			}
			else if ((modeInput.equals("4")))
			{
				mode = 4;
				System.out.println("So, you have figured out that difficulty increases as " +
							"numerical value increases.");
				System.out.println("Your dubious reward is this, the Extra Hard mode!");
				System.out.println("-insert evil laugh here-");
			}
			else
			{
				System.out.println("That is not a valid difficulty level.");
				System.out.println("Perhaps you should play a game " +
							" that does not require reading or typing.");
			}
		} while ((mode != 1) && (mode != 2) && (mode != 3) && (mode != 4));

		// The Medium, Hard, and Extra Hard levels include traps; this section places those traps.
		// Enemies for Hard and Extra Hard are placed here as well.
		// The do-while loop repeats so long as any objects occupy the same space.
		if (mode == 2)
		{
			do
			{
				trap0 = (100 * getRandom('x')) + (getRandom('y'));
			} while (((playerCoordinate == trap0) || (playerCoordinate == trap1)) ||
				(exitCoordinate == trap0));		
		}
		else if (mode == 3)
		{
			do
			{
				trap0 = (100 * getRandom('x')) + (getRandom('y'));
				trap1 = (100 * getRandom('x')) + (getRandom('y'));
			} while ((trap0 == trap1) || (playerCoordinate == trap0) || (playerCoordinate == trap1) ||
				(exitCoordinate == trap0) || (exitCoordinate == trap1));
	
			do
			{
				enemy0 = (100 * getRandom('x')) + (getRandom('y'));
			} while ((playerCoordinate == enemy0) && (exitCoordinate == enemy0));

			do
			{
				enemy1 = (100 * getRandom('x')) + (getRandom('y'));
			} while ((playerCoordinate == enemy1) || ((exitCoordinate / 100) == (enemy1 / 100)) ||
				(enemy0 == enemy1));			
			
			radarUses = 2;
		}
		else if (mode == 4)
		{
			do
			{
				trap0 = (100 * getRandom('x')) + (getRandom('y'));
				trap1 = (100 * getRandom('x')) + (getRandom('y'));
			} while ((trap0 == trap1) || (playerCoordinate == trap0) || (playerCoordinate == trap1) ||
				(exitCoordinate == trap0) || (exitCoordinate == trap1));
	
			do
			{
				enemy0 = (100 * getRandom('x')) + (getRandom('y'));
			} while ((playerCoordinate == enemy0) && (exitCoordinate == enemy0));

			do
			{
				enemy1 = (100 * getRandom('x')) + (getRandom('y'));
			} while ((playerCoordinate == enemy1) || ((exitCoordinate / 100) == (enemy1 / 100)) ||
				(enemy0 == enemy1));			
			
			rival = playerCoordinate;			

			radarUses = 2;
		}
		
	
		printIntroduction(mode);	

		while ((playerCoordinate != exitCoordinate) && (playDead == false))
		{
			// Yes, if the enemy lands on a trap, both enemy and trap vanish.  
			// And if the enemy walks into the exit, the enemy leaves the maze.
			// Aren't I nice to you?
			if (enemy0 == trap0)
			{
				enemy0 = -1;
				trap0 = -1;
			}
			else if (enemy0 == trap1)
			{
				enemy0 = -1;
				trap1 = -1;
			}
			else if (enemy0 == exitCoordinate)
			{
				enemy0 = -1;
			}
			if (enemy1 == trap0)
			{
				enemy1 = -1;
				trap0 = -1;
			}
			else if (enemy1 == trap1)
			{
				enemy1 = -1;
				trap1 = -1;
			}
			else if (enemy1 == exitCoordinate)
			{
				enemy1 = -1;
			}

			// The Rival suffers the same effects as the player for traps, monsters, etc.
			if (rival == exitCoordinate)
			{
				deathType = 2;
			}
			else if (rival == enemy0)
			{
				System.out.println("Somewhere far away, you hear a scream from your Rival.");
				System.out.println("Apparently, the Differential monster has caught him.");
				System.out.println("Your Rival collapses after a few seconds discussing vectors.");
				rival = -2;
			}
			else if (rival == enemy1)
			{
				System.out.println("A terrible shriek echoes throughout the maze.");
				System.out.println("You can barely hear the Integral monster talking.");
				System.out.println("Something about Greek mathemeticians using high school algebra...");
				System.out.println("You wisely decide to stay away.");
				rival = -2;
			}
			else if ((rival == playerCoordinate) && (counter != 0))
			{
				System.out.println("Your Rival is here!");
				System.out.println("Cue the theme music!");
				System.out.println("<http://www.steelydan.com/lyrgaucho.html#track6>");
			}

			printMaze(mode);

			// User types in command, program performs action
			System.out.println("Where do you want to move now?");
			userInput = input.nextLine();
			move(userInput);
			System.out.println("");			
			counter++;

			// The enemies do not move every single turn.
			// This makes survival a little bit easier for the player.
			if ((enemy0 >= 0) && ((counter % 3) == 0))
			{
				moveEnemy(0);
			}
			if ((enemy1 >= 0) && ((counter % 2) == 0))
			{
				moveEnemy(1);
			}

			if ((mode == 4) && (counter > bestMoves) && ((counter % 2) == 0))
			{
				moveExit();
			}

			if (rival >= 0)
			{
				moveRival();
			}

			// If enemy is in same room, determines which enemy for later sequence
			deathType = checkForEnemy();

			if (deathType != -1)
			{
				playDead = true;
			}
		}
		
		if (playDead == false)
		{
			victory(counter);
		}
		else if (playDead == true)
		{
			defeat(counter, deathType);
		}
	}

//-------------------------------------------------------------
/**
*	Method Name:	getRandom
*
*	Description:	Returns a random value for initial coordinates
*
*	@param 		char axis
*
*	@return		int value
*
*/

	public static int getRandom(char axis)
	{
		int value = 0;

		// Determines axis that value should be assigned to
		if (axis == 'x')
		{
			value = (int) (Math.random() * MAZE_WIDTH);
		}
		else if (axis == 'y')
		{
			value = (int) (Math.random() * MAZE_HEIGHT);
		}
		
		return (value);
	}

//-------------------------------------------------------------
/**
*	Method Name:	printIntroduction
*
*	Description:	Tells the user how to play the game
*
*	@param 		int difficulty
*
*/

	public static void printIntroduction(int difficulty)
	{
		System.out.println("This is a simplistic maze game.");
		System.out.println("On the map, your position is indicated with a \"P\".");
		System.out.println("Likewise, the exit is marked with an \"E\".");
		System.out.println("Normal floors are indicated by \"*\", " + 
					"and walls are signified  by \"-\" or \"|\".");
		System.out.println("You can move in any of the four cardinal directions " + 
					"(North, South, East, West)  by typing in " +
					"\"north,\" \"south,\" and so forth, and then pressing Enter");
		System.out.println("Alternately, if you are too lazy, you can just type in " +
					"\"n,\" \"s,\" and so forth   for the respective directions.");
		System.out.println("If you are too much of a wimp to get through this labyrinth by yourself, " +
					"you    may type in \"b\" to display the best move available.  " +
					"Second-hander.");
		System.out.println("If you need to refer to these directions again, " +
					"just type in \"help\"!");
		System.out.println("");

		if (difficulty != 1)
		{
			System.out.println("Additional Information:");
			
			if (difficulty == 2)
			{
				System.out.println("On this difficulty level, you can only see " +
							"three squares away from your current position.");

				System.out.println("This means that the position of the exit " +
							"is not known initially.");

				System.out.println("There is also one trap placed randomly in the maze.");
				System.out.println("This trap is indicated by a \"#\".");
				System.out.println("If you are caught in this trap, then " +
							"you will be unable to move for a few       turns.");

				System.out.println("When trapped, your position is noted with a \"d\".");
				System.out.println("To make life easier for you, you may access a radar " + 
							"by typing in \"r\" or \"radar\" as a command.");

				System.out.println("This will allow you a brief glance at the entire map.");
			}
			else if (difficulty == 3)
			{
				System.out.println("On this difficulty level, you can only see " +
							"one square away from your position.");

				System.out.println("This means that the position of the exit " +
							"is not known initially.");

				System.out.println("There are also two traps placed randomly in the maze.");
				System.out.println("These traps are indicated by a \"#\".");
				System.out.println("If you are caught in one of these traps, " +
							"you will be unable to move for a few   turns.");

				System.out.println("When trapped, your position is noted with a \"d\".");
				System.out.println("Furthermore, there are now two monsters roaming the maze.");
				System.out.println("These monsters are named \"Differential\" and \"Integral,\"" +
							"and are denoted with \"/\" and \"+\" respectively.");

				System.out.println("If these monsters ever catch up to you, they will attack " +
							"by talking until you   die of boredom.");

				System.out.println("To make life easier for you, you may access a radar " + 
							"by typing in \"r\" or \"radar\" as a command.");

				System.out.println("This will allow you a brief glance at the entire map.");
				System.out.println("However, you may only use the radar twice.");
			}	
			else if (difficulty == 4)
			{
				System.out.println("If you have found this difficulty level, " +
							"you have probably played through the   previous modes.");
				System.out.println("Everything from Hard Mode holds true here.");
				System.out.println("Traps are \"#\", the Differential monster is \"/\", " +
							"the Integral monster is \"+\", and radar is \"r\".");
				System.out.println("However, there are two additional challenges.");
				System.out.println("First, for every two moves beyond the minimum necessary " +
							"to complete the maze,   the exit will move one space.");
				System.out.println("Second, you are no longer the only one trying to get to the exit.");
				System.out.println("There is now another character, the Rival, who will join you.");
				System.out.println("If the Rival gets to the exit before you do, you lose.");
				System.out.println("This Rival is denoted with an \"R\", or \"r\" when trapped.");
				System.out.println("The Rival will always be visible on your map, " +
							"so if you get lost you can always try following him.");
				System.out.println("Do not worry if you fail... it is just a game.");
			}		
		}
	}

//-------------------------------------------------------------
/**
*	Method Name:	printMaze
*
*	Description:	Displays player position, exit, and maze with border
*			(Kids these days need pictures to do anything.  Wimps.)
*
*	@param 		int difficulty
*
*/

	public static void printMaze(int difficulty)
	{
		final String PLAYER = "P";
		final String EXIT = "E";
		final String FLOOR = "*";
		final String H_WALL = "-";
		final String V_WALL = "|";
		final String TRAP = "#";
		final String TRAPPED = "d";
		final String BLANK = " ";
		final String ENEMY0 = "/";
		final String ENEMY1 = "+";
		final String RIVAL = "R";
		final String R_TRAPPED = "r";
		
		printBorder('h', H_WALL);

		for (int y = 0; y < MAZE_WIDTH; y++)
		{	
			printBorder('v', V_WALL);
			for (int x = 0; x < MAZE_WIDTH; x++)
			{
				if ((x == (playerCoordinate / 100)) && (y == (playerCoordinate % 100)))
				{
					if ((playerCoordinate == trap0) || (playerCoordinate == trap1))
					{
						System.out.print(TRAPPED);
					}
					else
					{
						System.out.print(PLAYER);
					}
				}
				else if ((x == (rival / 100)) && (y == (rival % 100)))
				{
					if ((rival == trap0) || (rival == trap1))
					{
						System.out.print(R_TRAPPED);
					}
					else
					{
						System.out.print(RIVAL);
					}
				}
				else if ((x == (exitCoordinate / 100)) && (y == (exitCoordinate % 100)))
				{
					if (difficulty == 2)
					{
						if ((Math.abs((playerCoordinate / 100) - (exitCoordinate / 100)) <= 3) &&
							(Math.abs((playerCoordinate % 100) - (exitCoordinate % 100)) <= 3))
						{
							System.out.print(EXIT);
						}
						else
						{
							System.out.print(BLANK);
						}
					}
					else if ((difficulty == 3) || (difficulty == 4))
					{
						if ((Math.abs((playerCoordinate / 100) - (exitCoordinate / 100)) <= 1) &&
							(Math.abs((playerCoordinate % 100) - (exitCoordinate % 100)) <= 1))
						{
							System.out.print(EXIT);
						}
						else
						{
							System.out.print(BLANK);
						}
					}
					else
					{
						System.out.print(EXIT);
					}
				}
				else if ((x == (trap0 / 100)) && (y == (trap0 % 100)))
				{
					if (difficulty == 2)
					{
						if ((Math.abs((playerCoordinate / 100) - (trap0 / 100)) <= 3) &&
							(Math.abs((playerCoordinate % 100) - (trap0 % 100)) <= 3))
						{
							System.out.print(TRAP);
						}
						else
						{
							System.out.print(BLANK);
						}
					}
					else if ((difficulty == 3) || (difficulty == 4))
					{
						if ((Math.abs((playerCoordinate / 100) - (trap0 / 100)) <= 1) &&
							(Math.abs((playerCoordinate % 100) - (trap0 % 100)) <= 1))
						{
							System.out.print(TRAP);
						}
						else
						{
							System.out.print(BLANK);
						}
					}
					else
					{
						System.out.print(TRAP);
					}
				}
				else if ((x == (trap1 / 100)) && (y == (trap1 % 100))) 
				{
					if (difficulty == 2)
					{
						if ((Math.abs((playerCoordinate / 100) - (trap1 / 100)) <= 3) &&
							(Math.abs((playerCoordinate % 100) - (trap1 % 100)) <= 3))
						{
							System.out.print(TRAP);
						}
						else
						{
							System.out.print(BLANK);
						}
					}
					else if ((difficulty == 3) || (difficulty == 4))
					{
						if ((Math.abs((playerCoordinate / 100) - (trap1 / 100)) <= 1) && 
							(Math.abs((playerCoordinate % 100) - (trap1 % 100)) <= 1))
						{
							System.out.print(TRAP);
						}
						else
						{
							System.out.print(BLANK);
						}
					}
					else
					{
						System.out.print(TRAP);
					}
				}
				else if ((x == (enemy0 / 100)) && (y == (enemy0 % 100)))
				{
					if ((difficulty == 3) || (difficulty == 4))
					{
						if ((Math.abs((playerCoordinate / 100) - (enemy0 / 100)) <= 1) &&
							(Math.abs((playerCoordinate % 100) - (enemy0 % 100)) <= 1))
						{
							System.out.print(ENEMY0);
						}
						else	
						{
							System.out.print(BLANK);
						}
					}
					else
					{
						System.out.print(ENEMY0);
					}
				}
				else if ((x == (enemy1 / 100)) && (y == (enemy1 % 100)))
				{
					if ((difficulty == 3) || (difficulty == 4))
					{
						if ((Math.abs((playerCoordinate / 100) - (enemy1 / 100)) <= 1) && 
							(Math.abs((playerCoordinate % 100) - (enemy1 % 100)) <= 1))
						{
							System.out.print(ENEMY1);
						}
						else
						{
							System.out.print(BLANK);
						}
					}
					else
					{
						System.out.print(ENEMY1);
					}
				}
				else
				{
					if (difficulty == 2)
					{
						if ((Math.abs((playerCoordinate / 100) - x) <= 3) &&
							(Math.abs((playerCoordinate % 100) - y) <= 3))
						{
							System.out.print(FLOOR);
						}
						else
						{
							System.out.print(BLANK);
						}
					}
					else if ((difficulty == 3) || (difficulty == 4))
					{
						if ((Math.abs((playerCoordinate / 100) - x) <= 1) &&
							(Math.abs((playerCoordinate % 100) - y) <= 1))
						{
							System.out.print(FLOOR);
						}
						else
						{
							System.out.print(BLANK);
						}
					}
					else
					{
						System.out.print(FLOOR);
					}
				}
			}
			printBorder('v', V_WALL);
			printBorder('e', " ");
		}
		
		printBorder('h', H_WALL);
	}

//-------------------------------------------------------------
/**
*	Method Name:	printBorder
*
*	Description:	Prints borders around maze
*
*	@param 		char type, String wall
*
*/

	public static void printBorder(char type, String wall)
	{
		// If the wall displays horizontally; ie. along the top or bottom border
		if ((type == 'h') || (type == 'H'))
		{
			for (int h = 0; h < MAZE_WIDTH + 2; h++)
			{
				System.out.print(wall);
			}
			System.out.println("");
		}
		// If the wall displays vertically; ie. along the left or right border
		else if ((type == 'v') || (type == 'V'))
		{
			System.out.print(wall);
		}
		else
		{
			System.out.println(wall);
		}
	}

//-------------------------------------------------------------
/**
*	Method Name:	move
*
*	Description:	Changes the position of the player according to user input
*			If movement in a particular direction is not allowed, calls for error message
*
*	@param 		String userInput
*
*/

	public static void move(String userInput)
	{
		// For all movement, checks first if player is in a trap and cannot move
		// Then checks to ensure that player is not walking into a wall
		// Then if nothing is wrong, moves player

		if ((userInput.equalsIgnoreCase("n")) || (userInput.equalsIgnoreCase("north")))
		{
			if (((playerCoordinate == trap0) || (playerCoordinate == trap1)) && (trapCount != 0))
			{
				System.out.println("You are trapped and cannot move!");
				trapCount--;
			}
			else if ((playerCoordinate % 100) == 0)
			{
				bonk();
			}
			else
			{
				playerCoordinate -= 1;
				if ((playerCoordinate == trap0) || (playerCoordinate == trap1))
				{
					System.out.println("You step into a trap!");
					if (d == true)
					{
						System.out.println("However, the trap is no longer working.");
						System.out.println("You are free to move on.");
					}
					else
					{
						trapCount = 4;
					}
				}
			}
		}
		else if ((userInput.equalsIgnoreCase("s")) || (userInput.equalsIgnoreCase("south")))
		{
			if (((playerCoordinate == trap0) || (playerCoordinate == trap1)) && (trapCount != 0))
			{
				System.out.println("You are trapped and cannot move!");
				trapCount--;
			}
			else if ((playerCoordinate % 100) == MAZE_HEIGHT - 1)
			{
				bonk();
			}
			else
			{
				playerCoordinate += 1;
				if ((playerCoordinate == trap0) || (playerCoordinate == trap1))
				{
					System.out.println("You step into a trap!");
					if (d == true)
					{
						System.out.println("However, this trap is no longer operable.");
						System.out.println("You may proceed.");
					}
					else
					{
						trapCount = 4;
					}
				}
			}
		}
		else if ((userInput.equalsIgnoreCase("e")) || (userInput.equalsIgnoreCase("east")))
		{
			if (((playerCoordinate == trap0) || (playerCoordinate == trap1)) && (trapCount != 0))
			{
				System.out.println("You are trapped and cannot move!");
				trapCount--;
			}
			else if ((playerCoordinate / 100) == MAZE_WIDTH - 1)
			{
				bonk();
			}
			else
			{
				playerCoordinate += 100;
				if ((playerCoordinate == trap0) || (playerCoordinate == trap1))
				{
					System.out.println("You step into a trap!");
					if (d == true)
					{
						System.out.println("This trap seems to be broken though.");
						System.out.println("You may continue walking.");
					}
					else
					{
						trapCount = 4;
					}
				}
			}
		}
		else if ((userInput.equalsIgnoreCase("w")) || (userInput.equalsIgnoreCase("west")))
		{
			if (((playerCoordinate == trap0) || (playerCoordinate == trap1)) && (trapCount != 0))
			{
				System.out.println("You are trapped and cannot move!");
				trapCount--;
			}
			else if ((playerCoordinate / 100) == 0)
			{
				bonk();
			}
			else
			{
				playerCoordinate -= 100;
				if ((playerCoordinate == trap0) || (playerCoordinate == trap1))
				{
					System.out.println("You step into a trap!");
					if (d == true)
					{
						System.out.println("Someone has disabled this trap for you though.");
						System.out.println("Be thankful.");
					}
					else
					{
						trapCount = 4;
					}
				}
			}
		}
		else if ((userInput.equalsIgnoreCase("h")) || (userInput.equalsIgnoreCase("help")))
		{
			printIntroduction(1);
		}
		else if ((userInput.equalsIgnoreCase("r")) || (userInput.equalsIgnoreCase("radar")))
		{
			if (radarUses != 0)
			{
				printMaze(1);
				radarUses--;
			}
			else
			{
				System.out.println("You cannot use the radar any more.");
			}
		}
		else if ((userInput.equalsIgnoreCase("b")) || (userInput.equalsIgnoreCase("best")))
		{
			bestMove();
		}
		// Cheats make the world go round!
		else if ((userInput.equalsIgnoreCase("m")) || (userInput.equalsIgnoreCase("mu")))
		{
			// To instantly win the game
			System.out.println("You probably want to go get some ice cream, right?");
			System.out.println("Well then, I suppose that we can end this quickly.");
			playerCoordinate = exitCoordinate;
		}
		else if ((userInput.equalsIgnoreCase("a")) || (userInput.equalsIgnoreCase("alpha")))
		{
			// To become invulnerable to all enemies
			System.out.println("You are now immune to any and all enemy attacks.");
			e = true;	
		}
		else if ((userInput.equalsIgnoreCase("g")) || (userInput.equalsIgnoreCase("gamma")))
		{
			// Disables all traps in the maze
			System.out.println("All of the traps in this maze are now deactivated.");
			d = true;
		}
		// Things besides cheats make the world go flat!
		// (In other words, the cheat section is over.)
		else
		{
			System.out.println("Bad command or file name");
			System.out.println("Um... I mean...");
			System.out.println("\"" + userInput + "\" is not a valid command.  Please try again.");
		}
	}

//--------------------------------------------------------------
/**
*	Method Name:	bonk
*
*	Description:	Displays message when user runs into a "wall"
*
*/

	public static void bonk()
	{
		int crash = (int) (Math.random() * 8);
		switch (crash)
		{
			case 0:	
				System.out.println("You cannot go that direction.");
				break;
			case 1:
				System.out.println("In a moment of sheer brilliance, you walk directly into a solid wall!");
				break;
			case 2:
				System.out.println("Ow!");
				break;
			case 3:
				System.out.println("As insanity closes in, you begin to mistake walls for doorways.");
				break;
			case 4: 
				System.out.println("What are you going to do for an encore, fall down a flight of stairs?");
				break;
			case 5:
				System.out.println("I bet it hurts smashing your face into a wall.");
				break;
			case 6:
				System.out.println("There is not a door there!");
				break;
			case 7:
				System.out.println("Are you going blind?  There is a wall there, you know.");
				break;
		}
	}

//--------------------------------------------------------------
/**
*	Method Name:	bestMove
*
*	Description:	Calculates the best possible move at that point in the game
*			Or maybe it just gives back a random answer; who knows?
*	
*/

	public static void bestMove()
	{
		char best = ' ';
		char direction = ' ';

		if (((playerCoordinate % 100) > (exitCoordinate % 100))) 
		{
			if ((Math.abs(((playerCoordinate - 1) % 100) - (enemy0 % 100)) <= 1) ||
				(Math.abs(((playerCoordinate - 1) % 100) - (enemy1 % 100)) <= 1))
			{
				best = 'm';
			}
			else if (((playerCoordinate - 1) == trap0) || ((playerCoordinate - 1) == trap1))
			{
				best = 't';
			}
			else
			{
				best = 'n';
			}
			direction = 'n';
		}
		else if (((playerCoordinate % 100) < (exitCoordinate % 100)))
		{
			if ((Math.abs(((playerCoordinate + 1) % 100) - (enemy0 % 100)) <= 1) || 
				(Math.abs(((playerCoordinate + 1) % 100) - (enemy1 % 100)) <= 1))
			{
				best = 'm';
			}
			else if (((playerCoordinate + 1) == trap0) || ((playerCoordinate + 1) == trap1))
			{
				best = 't';
			}
			else
			{
				best = 's';
			}
			direction = 's';
		}
		else if (((playerCoordinate / 100) < (exitCoordinate / 100))) 
		{
			if ((Math.abs(((playerCoordinate + 100) / 100) - (enemy0 / 100)) <= 1) ||
				(Math.abs(((playerCoordinate + 100) / 100) - (enemy1 / 100)) <= 1))
			{
				best = 'm';
			}				
			else if (((playerCoordinate + 100) == trap0) || ((playerCoordinate + 100) == trap1))
			{
				best = 't';
			}
			else
			{
				best = 'e';
			}
			direction = 'e';
		}
		else if (((playerCoordinate / 100) > (exitCoordinate / 100)))
		{
			if ((Math.abs(((playerCoordinate - 100) / 100) - (enemy0 / 100)) <= 1) || 
				(Math.abs(((playerCoordinate - 100) / 100) - (enemy1 / 100)) <= 1))
			{
				best = 'm';
			}
			else if (((playerCoordinate - 100) == trap0) || ((playerCoordinate - 100) == trap1))
			{
				best = 't';
			}
			else
			{
				best = 'w';
			}
			direction = 'w';
		}
	
		if (best == 'm')
		{
			if (direction == 'n')
			{
				if ((Math.abs(((playerCoordinate + 1) % 100) - (enemy0 % 100)) > 1) ||
					(Math.abs(((playerCoordinate + 1) % 100) - (enemy1 % 100)) > 1))
				{
					best = 's';
				}
				else if ((Math.abs(((playerCoordinate + 100) / 100) - (enemy0 / 100)) > 1) ||
					(Math.abs(((playerCoordinate + 100) / 100) - (enemy1 / 100)) > 1))
				{
					best = 'e';
				}
				else if ((Math.abs(((playerCoordinate - 100) / 100) - (enemy0 / 100)) > 1) ||
					(Math.abs(((playerCoordinate - 100) / 100) - (enemy1 / 100)) > 1))
				{
					best = 'w';
				}
				else
				{
					best = 't';
				}
			}
			else if (direction == 's')
			{
				if ((Math.abs(((playerCoordinate - 1) % 100) - (enemy0 % 100)) > 1) ||
					(Math.abs(((playerCoordinate - 1) % 100) - (enemy1 % 100)) > 1))
				{
					best = 'n';
				}
				else if ((Math.abs(((playerCoordinate + 100) / 100) - (enemy0 % 100)) > 1) ||
					(Math.abs(((playerCoordinate + 100) / 100) - (enemy1 % 100)) > 1))
				{
					best = 'e';
				}
				else if ((Math.abs(((playerCoordinate - 100) / 100) - (enemy0 / 100)) > 1) ||
					(Math.abs(((playerCoordinate - 100) / 100) - (enemy1 / 100)) > 1))
				{
					best = 'w';
				}
				else
				{
					best = 't';
				}
			}
			else if (direction == 'e')
			{
				if ((Math.abs(((playerCoordinate - 1) % 100) - (enemy0 % 100)) > 1) ||
					(Math.abs(((playerCoordinate - 1) % 100) - (enemy1 % 100)) > 1))
				{
					best = 'n';
				}
				else if ((Math.abs(((playerCoordinate + 1) % 100) - (enemy0 % 100)) > 1) ||
					(Math.abs(((playerCoordinate + 1) % 100) - (enemy1 % 100)) > 1))
				{
					best = 's';
				}
				else if ((Math.abs(((playerCoordinate - 100) / 100) - (enemy0 / 100)) > 1) ||
					(Math.abs(((playerCoordinate - 100) / 100) - (enemy1 / 100)) > 1))
				{
					best = 'w';
				}
				else
				{
					best = 't';
				}
			}
			else if (direction == 'w')
			{
				if ((Math.abs(((playerCoordinate - 1) % 100) - (enemy0 % 100)) > 1) ||
					(Math.abs(((playerCoordinate - 1) % 100) - (enemy1 % 100)) > 1))
				{
					best = 'n';
				}
				else if ((Math.abs(((playerCoordinate + 1) % 100) - (enemy0 % 100)) > 1) ||
					(Math.abs(((playerCoordinate + 1) % 100) - (enemy1 % 100)) > 1))
				{
					best = 's';
				}
				else if ((Math.abs(((playerCoordinate + 100) / 100) - (enemy0 / 100)) > 1) ||
					(Math.abs(((playerCoordinate + 100) / 100) - (enemy1 / 100)) > 1))
				{
					best = 'e';
				}
				else
				{
					best = 't';
				}
			}
		}

		if (best == 't')
		{
			if (direction == 'n')
			{
				if (((playerCoordinate + 1) != trap0) && 
					((playerCoordinate + 1) != trap1) && 
					((playerCoordinate % 100) < (exitCoordinate % 100)))
				{
					best = 's';
				}
				else if (((playerCoordinate + 100) != trap0) && 
					((playerCoordinate + 100) != trap1) &&
					((playerCoordinate / 100) < (exitCoordinate / 100)))
				{
					best = 'e';
				}
				else if (((playerCoordinate - 100) != trap0) && 
					((playerCoordinate - 100) != trap1) &&
					((playerCoordinate / 100) > (exitCoordinate / 100)))
				{
					best = 'w';
				}
				else
				{
					best = '?';
				}
			}
			else if (direction == 's')
			{
				if (((playerCoordinate - 1) != trap0) && 
					((playerCoordinate - 1) != trap1) &&
					((playerCoordinate % 100) > (exitCoordinate % 100)))
				{
					best = 'n';
				}
				else if (((playerCoordinate + 100) != trap0) && 
					((playerCoordinate + 100) != trap1) &&
					((playerCoordinate / 100) < (exitCoordinate / 100)))
				{
					best = 'e';
				}
				else if (((playerCoordinate - 100) != trap0) && 
					((playerCoordinate - 100) != trap1) &&
					((playerCoordinate / 100) > (exitCoordinate / 100)))
				{
					best = 'w';
				}
				else
				{
					best = '?';
				}
			}
			else if (direction == 'e')
			{
				if (((playerCoordinate - 1) != trap0) && 
					((playerCoordinate - 1) != trap1) && 
					((playerCoordinate % 100) > (exitCoordinate % 100)))
				{
					best = 'n';
				}
				else if (((playerCoordinate + 1) != trap0) && 
					((playerCoordinate + 1) != trap1) &&
					((playerCoordinate % 100) < (exitCoordinate % 100)))
				{
					best = 's';
				}
				else if (((playerCoordinate - 100) != trap0) && 
					((playerCoordinate - 100) != trap1) &&
					((playerCoordinate / 100) > (exitCoordinate / 100)))
				{
					best = 'w';
				}
				else
				{
					best = '?';
				}
			}
			else if (direction == 'w')
			{
				if (((playerCoordinate - 1) != trap0) && 
					((playerCoordinate - 1) != trap1) &&
					((playerCoordinate % 100) > (exitCoordinate % 100)))
				{
					best = 'n';
				}
				else if (((playerCoordinate + 1) != trap0) && 
					((playerCoordinate + 1) != trap1) &&
					((playerCoordinate % 100) < (exitCoordinate % 100)))
				{
					best = 's';
				}
				else if (((playerCoordinate + 100) != trap0) && 
					((playerCoordinate + 100) != trap1) &&
					((playerCoordinate / 100) < (exitCoordinate / 100)))
				{
					best = 'e';
				}
				else
				{
					best = '?';
				}
			}
		}					

		System.out.print("Your best move is ");
		if (best == 'n')
		{
			System.out.println("north.");
		}
		else if (best == 's')
		{
			System.out.println("south.");
		}
		else if (best == 'e')
		{
			System.out.println("east.");
		}
		else if (best == 'w')
		{
			System.out.println("west.");
		}
		else
		{
			System.out.println("to scream and run away.");
		}
	}

//--------------------------------------------------------------
/**
*	Method Name:	moveEnemy
*
*	Description:	If the game is on Hard or Extra Hard difficulty, there will be two monsters in the maze
*			This handles all movement for said monsters
*			Anyone who correctly guesses who these monsters are based on gets a free cookie.
*
*	@param 		int enemyType
*
*/

	public static void moveEnemy(int enemyType)
	{
		if (enemyType == 0)
		{
			if (enemy0 == playerCoordinate)
			{
				// Do nothing, since the enemy and player are in the same room
			}
		
			// These will generally move enemy0 closer to the player
			else if ((enemy0 / 100) > (playerCoordinate / 100))
			{
				enemy0 -= 100;
			}
			else if ((enemy0 / 100) < (playerCoordinate / 100))
			{
				enemy0 += 100;
			}
			else if ((enemy0 % 100) > (playerCoordinate % 100))
			{
				enemy0 -= 1;
			}
			else if ((enemy0 % 100) < (playerCoordinate % 100))
			{
				enemy0 += 1;
			}
		}
		else if (enemyType == 1)
		{
			if (enemy1 == playerCoordinate)
			{
				// Do nothing, since the enemy and player are in the same room
			}
			
			// If enemy1 is within a three-space radius of the player, 
			// he will move closer to the player
			else if ((Math.abs((playerCoordinate / 100) - (enemy1 / 100)) <= 3) ||
				(Math.abs((playerCoordinate % 100) - (enemy1 % 100)) <= 3))
			{
				if ((enemy1 % 100) > (playerCoordinate % 100))
				{
					enemy1 -= 1;
				}
				else
				{
					enemy1 += 1;
				}
			}
			// If enemy1 is not within three spaces of the player, he moves randomly
			else
			{
				int direction = (int) (Math.random() * 4);
				if ((direction == 0) && ((enemy1 % 100) != 0))
				{
					enemy1 -= 1;
				}
				else if ((direction == 1) && ((enemy1 % 100) != (MAZE_HEIGHT - 1)))
				{
					enemy1 += 1;
				}
				else if ((direction == 2) && ((enemy1 / 100) != (MAZE_WIDTH - 1)))
				{
					enemy1 += 100;
				}
				else if ((direction == 3) && ((enemy1 / 100) != 0))
				{
					enemy1 -= 100;
				}
			}
		}
	}

//--------------------------------------------------------------
/**
*	Method Name:	moveExit
*
*	Description:	On Extra Hard difficulty, the exit will move every two turns 
*			after the minimum required to complete the maze.
*			This method moves the exit in a "predictable manner"
*
*/
	
	public static void moveExit()
	{	
		// Corners
		if (((exitCoordinate / 100) == 0) && ((exitCoordinate % 100) == 0))
		{
			exitCoordinate += 1;
		}
		else if (((exitCoordinate / 100) == 0) && ((exitCoordinate % 100) == (MAZE_HEIGHT - 1)))
		{
			exitCoordinate += 100;
		}
		else if (((exitCoordinate / 100) == (MAZE_WIDTH - 1)) && ((exitCoordinate % 100) == (MAZE_HEIGHT - 1)))
		{
			exitCoordinate -= 1;
		}
		else if (((exitCoordinate / 100) == (MAZE_WIDTH - 1)) && ((exitCoordinate % 100) == 0))
		{
			exitCoordinate -= 100;
		}
		// Walls
		else if ((exitCoordinate / 100) == 0)
		{
			exitCoordinate += 1;
		}
		else if ((exitCoordinate % 100) == (MAZE_HEIGHT - 1))
		{
			exitCoordinate += 100;
		}
		else if ((exitCoordinate / 100) == (MAZE_WIDTH - 1))
		{
			exitCoordinate -= 1;
		}
		else if ((exitCoordinate % 100) == 0)
		{
			exitCoordinate -= 100;
		}
	}
			

//--------------------------------------------------------------
/**
*	MethodName:	checkForEnemy
*
*	Description:	Determines if any enemy is in the same room as the player
*			If so, the player is "dead."
*			Normally.
*
*	@return		int deathType
*
*/

	public static int checkForEnemy()
	{
		int deathType = -1;
		if (playerCoordinate == enemy0)
		{
			if (e == true)
			{
				System.out.println("The Differential monster has caught up to you!");
				System.out.println("However, his words cannot harm you.");
				System.out.println("After all, only certain people can wear pink hair ribbons.");
			}
			else
			{
				deathType = 0;
			}
		}
		else if (playerCoordinate == enemy1)
		{
			if (e == true)
			{
				System.out.println("The Integral monster has caught up to you!");
				System.out.println("However, a sudden burst of rain drowns out his voice.");
				System.out.println("Still, it might be wise to run away before the rain stops.");
			}
			else
			{
				deathType = 1;
			}
		}
		
		return(deathType);
	}

//--------------------------------------------------------------
/**
*	Method Name:	moveRival
*
*	Description:	On Extra Hard, this will direct the Rival around the maze
*			This is not necessarily in a logical fashion
*
*/

	public static void moveRival()
	{		
		if (((rival != trap0) || (rival != trap1)) && (rivalTrapCount == 0))
		{
			if ((rival - 1) == exitCoordinate)
			{
				rival -= 1;
			}
			else if ((rival + 1) == exitCoordinate)
			{
				rival += 1;
			}
			else if ((rival + 100) == exitCoordinate)
			{
				rival += 100;
			}
			else if ((rival - 100) == exitCoordinate)
			{
				rival -= 100;
			}
			else
			{
				int direction = (int) (Math.random() * 4);
				if (direction == 0)
				{
					if ((rival % 100) != 0)
					{
						rival -= 1;
					}
					else
					{
						rival += 1;
					}
				}
				else if (direction == 1) 
				{
					if ((rival % 100) != (MAZE_HEIGHT - 1))
					{
						rival += 1;
					}
					else 
					{
						rival -= 1;
					}
				}
				else if (direction == 2) 
				{
					if ((rival / 100) != (MAZE_WIDTH - 1))
					{
						rival += 100;
					}
					else
					{
						rival -= 100;
					}
				}
				else if (direction == 3) 
				{
					if ((rival / 100) != 0)
					{
						rival -= 100;
					}
					else
					{
						rival += 100;
					}
				}
			}

			if ((rival == trap0) || (rival == trap1))
			{
				rivalTrapCount = 4;
			}
		}
		else
		{
			rivalTrapCount--;
		}
	}

//--------------------------------------------------------------
/**
*	Method Name:	victory
*
*	Description:	Displays message when user reaches the exit
*			If the user actually wins, then I as the author was not cruel enough.
*
*	@param 		int count
*
*/

	public static void victory(int count)
	{
		System.out.print("Congratulations!  You completed the maze in " + count + " move");
		if (count != 1)
		{
			System.out.print("s");
		}
		System.out.println("!");

		if (count > bestMoves)
		{
			System.out.println("This is " + (count - bestMoves) + " more than the minimum to finish the maze.");
		}
		else if (count == bestMoves)
		{
			System.out.println("You completed the maze in the fewest amount of moves possible!");
			System.out.println("That means that you are more efficient than my code is.");
		}
		else
		{
			System.out.println("Something is wrong here.  You must have cheated.");
			System.out.println("The fewest amount of moves necessary to complete the maze was " + 
						bestMoves + ".");
		}
	}		

//--------------------------------------------------------------
/**
*	Method Name:	defeat
*
*	Description:	Displays message if the user does not reach the exit
*
*	@param 		int count
*
*/

	public static void defeat(int count, int type)
	{
		// These monsters are not that dangerous... right?
		// So please do not file lawsuits against me alleging that I have traumatized you or anything.
		if (type == 0)
		{
			System.out.println("The Differential monster has caught up to you!");
			System.out.println("He mentions something about Physics and how he is being pendantic.");
			System.out.println("That is all you hear before you fall unconscious from boredom.");
			System.out.println("");
		}
		else if (type == 1)
		{
			System.out.println("The Integral monster has caught up to you!");
			System.out.println("Given that he is, after all, named \"Integral,\" " +
						"you would expect him to          integrate you by parts or something.");
			System.out.println("However, he seems quite content to ramble on about high school algebra.");
			System.out.println("You lose track of his discourse somewhere after he mentions Leibniz.");
			System.out.println("Just as you begin to drift into sleep, he runs up to you " +
						"and starts bobbing his head wildly.");
			System.out.println("");
		}
		else if (type == 2)
		{
			System.out.println("In the distance, you can hear maniacal laughter " +
						"as your rival reaches the exit.");
		}

		System.out.println("Oh dear.  You seem to be quite dead.");
		System.out.println("You survived for " + count + " turns.");
	}
}